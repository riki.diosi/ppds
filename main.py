#Základ programu prebraný zo semináru :
#https://www.youtube.com/watch?v=SJyUawNjpf0&feature=youtu.be&ab_channel=Paraleln%C3%A9programovanieadistribuovan%C3%A9syst%C3%A9my
#Program má za úlohu zvýšiť hodnotu zdielaneho pola, tak aby kazdy prvok bol zvýšený z hodnoty 0 na hodnotu 1
#Program obsahuje jednu triedu Shared, ktorá slúži na zdielanie dát medzi vláknami, čím sme sa vyhli použitiu globálnych premenných
#Našou úlohou je základný program velpšiť tak aby fungoval aj pre polia s velkosťou > 1000 000. 
#Problém so základnou verziou programu je ten, že pri veľkom počte iterácií sa jednotlivé vlákna nedokážu zosynchronizovať a dochádza k pretečeniu pola alebo neinkrementovaniu všetkých hodnôt pola. 
#Na riešenie nášho problému sme použili synchonizačný nástroj mutex s použitím metód na uzamknutie a odomknutie vlákien
from fei.ppds import Thread, Mutex


class Shared():
    def __init__(self, end):
        self.counter = 0
        self.end = end
        self.array = [0] * self.end
        self.mutex = Mutex()


class Histogram(dict):
    def __init__(self, seq = []):
        for item in seq:
            self[item] = self.get(item, 0) + 1
            
#Prípad A - funkčné riešenie kedy pred samotnou kontrolou, či sa nachádzame v rozsahu veľkosti poľa uzamkneme vlákno, kedy sa k 
# danej vetve if nedostane druhé vlákno, čím zabezpečíme potrebnú synchonizáciu a funkčnosť programu.

#Prípad B - Nefunkčné riešenie : Keďže v tomto prípade uzamkneme vlákno až po tom ako skontrolujeme, či sa nachádzame v rozsahu pola, a teda v prípade, že už jedno vlákno zvyšilo hodnotu posledného indexu, čím sme sa dostali na koniec pola, môže nastať situácia, že aj druhé vlákno zvýši daný index a tým na pole pretečie(IndexError: list index out of range).

#Prípad C - obdobne riešenie ako pri prípade B, s tým rozdielom, že sa nedostaneme mimo rozsah pola, keďže uzamkneme len časť pre 
# zvýšenie počítadla tak nám program nespadne ale nefunguje správne keďže ako som spomenul vyššie, že jedno vlákno už môže byť
# na konci pola a vykoná sa break a druhé vlákno pokračuje ďalej, čim vzniknú chyby v programe a nie všetky hodnoty pola sa nám # zvýšia ale ostanú na inicializačnej honote 0. 
def counter(shared):
    while True:
        shared.mutex.lock() #A
        if shared.counter >= shared.end:
            shared.mutex.unlock() #A # odomknutie vlákna z dôvodu toho, že v prípade ked sa nachádzame na konci pola by sa nám 
            break # vykonal príkaz break, ktorý by nás vyhodil z while cyklu čím by došlo k spadnutiu programu (deadlock)
        shared.mutex.lock() #B 
        shared.array[shared.counter] += 1
        shared.mutex.lock() #C
        shared.counter += 1
        shared.mutex.unlock() #A #B #C #odomknutie vlákna pre pokračovanie chodu programu v cykle. 
  
for _ in range(10):
    sh = Shared(1000000)
    t1 = Thread(counter, sh)
    t2 = Thread(counter, sh)

    t1.join()
    t2.join()

    print(Histogram(sh.array))

